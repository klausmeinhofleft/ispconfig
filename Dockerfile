FROM debian:stable-slim

MAINTAINER HacKan <hackan@rlab.be> version 0.5
# IPSConfig only, w/o mail, dns, nor anything else
# Based on https://www.howtoforge.com/tutorial/perfect-server-debian-9-stretch-apache-bind-dovecot-ispconfig-3-1/
# And https://github.com/jerob/docker-ispconfig

# Let the container know that there is no tty
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get upgrade -y -qq
RUN apt-get install -y -qq bash wget python haveged openssh-server

# Change the default shell
RUN echo "dash  dash/sh boolean no" | debconf-set-selections \
    && dpkg-reconfigure dash

# Jailkit
RUN apt-get install -y -qq build-essential autoconf automake libtool flex bison debhelper binutils
RUN mkdir /tmp/jailkit
RUN wget --no-verbose -O /tmp/jailkit/jailkit.tar.gz https://olivier.sessink.nl/jailkit/jailkit-2.19.tar.gz
RUN wget --no-verbose -O /tmp/jailkit/jailkit.tar.gz.sig https://olivier.sessink.nl/jailkit/jailkit-2.19.tar.gz.sig
RUN gpg --keyserver hkp://ipv4.pool.sks-keyservers.net --recv-keys 26073EFCDAC576E6 && gpg --trust-model always --verify /tmp/jailkit/jailkit.tar.gz.sig /tmp/jailkit/jailkit.tar.gz
RUN cd /tmp/jailkit \
    && tar -xzf jailkit.tar.gz \
    && cd jailkit-* \
    && echo 5 > debian/compat \
    && ./debian/rules binary \
    && cd .. \
    && dpkg -i jailkit_2*.deb
RUN apt-get remove --purge -y -qq build-essential autoconf automake libtool flex bison debhelper

# Install required stuff
RUN apt-get -y -qq install \
    mariadb-client \
    openssl rkhunter \
    binutils apache2 \
    apache2-utils \
    libapache2-mod-php \
    php7.0 \
    php7.0-common \
    php7.0-gd \
    php7.0-mysql \
    php7.0-imap \
    phpmyadmin \
    php7.0-cli \
    php7.0-cgi \
    libapache2-mod-fcgid \
    apache2-suexec-pristine \
    php-pear \
    php7.0-mcrypt \
    mcrypt \
    imagemagick \
    libruby \
    libapache2-mod-python \
    php7.0-curl \
    php7.0-intl \
    php7.0-pspell \
    php7.0-recode \
    php7.0-sqlite3 \
    php7.0-tidy \
    php7.0-xmlrpc \
    php7.0-xsl \
    memcached \
    php-memcache \
    php-imagick \
    php-gettext \
    php7.0-zip \
    php7.0-mbstring \
    libapache2-mod-passenger \
    php7.0-soap \
    php7.0-fpm \
    php7.0-opcache \
    php-apcu \
    quota \
    quotatool \
    webalizer \
    fail2ban \
    rsyslog \
    rsyslog-relp \
    logrotate \
    supervisor \
    vlogger

# Prevent HTTPOXY
RUN mkdir -p /etc/apache2/conf-available/ \
    && printf "%s\n\t%s\n%s\n" "<IfModule mod_headers.c>" "RequestHeader unset Proxy early" "</IfModule>" >> /etc/apache2/conf-available/httpoxy.conf

# Enable Apache modules
RUN a2enmod \
    suexec \
    rewrite \
    ssl \
    actions \
    include \
    dav_fs \
    dav \
    auth_digest \
    cgi \
    headers \
    actions \
    proxy_fcgi \
    alias \
    setenvif \
    && service apache2 restart \
    && a2enconf httpoxy \
        php7.0-fpm \
    && service apache2 reload

# Clean
RUN apt-get autoremove -y -qq \
    && apt-get clean -y -qq \
    && rm -rf /tmp/* > /dev/null 2>&1

COPY ispconfig.bash /root/
COPY autoinstall.ini /root/
COPY supervisord.conf /etc/supervisor/supervisord.conf

VOLUME ["/var/www/","/var/backup/","/var/lib/mysql","/var/log/"]
EXPOSE 22/tcp 80/tcp 8080/tcp

# Install ISPConfig or just run
# It can't be installed prior mounting volumes so it must be from a script
CMD ["/bin/bash", "/root/ispconfig.bash"]
