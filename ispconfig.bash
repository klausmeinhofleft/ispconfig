#!/bin/bash
# ISPConfig installer & runner
# by HacKan (https://hackan.net) under GNU GPL v3.0+

# Version 0.8

declare -r DB_DUMPFILE="/var/backup/sql/dbdump-$(printf "%s" "$RANDOM" | md5sum --tag - | tail -q -z -c 5)-$(date +"%Y%m%d_%H%M%S").sql"
declare -r TMPDIR="$(mktemp -d --tmpdir 'ispconfig.XXXXX')"
declare -r INSTALL_LOG="/root/ispconfig.installed"
declare -a DIRS=( /var/log/apache2 /var/www/html /var/run/sshd /var/log/supervisor /var/run/supervisor /var/backup/sql )
declare -r ISPCONFIG_PACKAGE_URL='https://ispconfig.org/downloads/ISPConfig-3.1.13.tar.gz'

declare -r MYSQL_DATABASE="${MYSQL_DATABASE:-dbispconfig}"
declare -r MYSQL_USER="${MYSQL_USER:-ispconfig}"

error() {
    echo -e "ERROR: $*" >&2
}

bailout() {
    [[ -n "$*" ]] && error "$*"
    echo "Bailing out..." >&2
    exit 1
}

download_files() {
    local sha512hash='614abe6943df260a715396f10fe1ae57263141895aa4c3366bc7bc7d2782aa856ce94bf5bdc30152238cc0cf8859ecf953465eebe6bf83442af1c0c0e666f8fb'
    local tarfile="$TMPDIR/ISPConfig-3.tar.gz"

    echo "Downloading and validating ISPConfig..."
    wget -O "$tarfile" --no-verbose "$ISPCONFIG_PACKAGE_URL"
    if ! printf "%s\\n" "$sha512hash  $tarfile" | sha512sum -c --status -; then
        error "the SHA512 hash validation fails for the downloaded file!"
        error "\\tExpected hash: $sha512hash"
        bailout "\\tCurrent hash: $(sha512sum "$tarfile")"
    fi
    tar -xzf "$tarfile" -C "$TMPDIR"
}

backup_db_and_drop() {
    if mysqlshow -h"$MYSQL_HOST" -P"$MYSQL_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD" | grep "$MYSQL_DATABASE" > /dev/null; then
        echo "Database detected!"
        # It could be the empty DB set by MariaDB
        local tables="$(printf "%s\\n" "show tables;" | mysql -h"$MYSQL_HOST" -P"$MYSQL_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE")"
        if [[ -z "$tables" ]]; then
            echo "Database is empty"
        else
            echo "Dumping database in $DB_DUMPFILE ..."
            mysqldump -h"$MYSQL_HOST" -P"$MYSQL_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE" > "$DB_DUMPFILE"
        fi
        echo "Dropping DB for the instalation to succeed..."
        printf "%s\\n" "drop database $MYSQL_DATABASE;" | mysql -h"$MYSQL_HOST" -P"$MYSQL_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD"
    fi
}

set_install_options() {
    local INI="$1"
    cp /root/autoinstall.ini "$INI"
    sed -i "s/^hostname=server1.example.com$/hostname=$HOSTNAME/g" "$INI"
    sed -i "s/^ssl_cert_country=AR$/ssl_cert_country=${SSLCERT_COUNTRY:-AR}/g" "$INI"
    sed -i "s/^ssl_cert_state=BuenosAires$/ssl_cert_state=${SSLCERT_STATE:-Buenos Aires}/g" "$INI"
    sed -i "s/^ssl_cert_locality=CABA$/ssl_cert_locality=${SSLCERT_LOCALITY:-CABA}/g" "$INI"
    sed -i "s/^ssl_cert_organisation=Rlyeh$/ssl_cert_organisation=${SSLCERT_ORG:-Rlyeh}/g" "$INI"
    sed -i "s/^ssl_cert_organisation_unit=Sysadmins$/ssl_cert_organisation_unit=${SSLCERT_ORG_UNIT:-Sysadmins}/g" "$INI"
    sed -i "s/^ssl_cert_common_name=server1.example.com$/ssl_cert_common_name=${SSLCERT_COMMON_NAME:-ispconfig}/g" "$INI"
    sed -i "s/^mysql_ispconfig_user=admin$/mysql_ispconfig_user=$MYSQL_USER/g" "$INI"
    sed -i "s/^mysql_ispconfig_password=admin$/mysql_ispconfig_password=$MYSQL_PASSWORD/g" "$INI"
    sed -i "s/^mysql_master_hostname=localhost$/mysql_master_hostname=$MYSQL_HOST/g" "$INI"
    sed -i "s/^mysql_hostname=localhost$/mysql_hostname=$MYSQL_HOST/g" "$INI"
    sed -i "s/^mysql_master_root_password=pass$/mysql_master_root_password=$MYSQL_ROOT_PASSWORD/g" "$INI"
    sed -i "s/^mysql_root_password=pass$/mysql_root_password=$MYSQL_ROOT_PASSWORD/g" "$INI"
    sed -i "s/^mysql_master_database=dbispconfig$/mysql_master_database=$MYSQL_DATABASE/g" "$INI"
    sed -i "s/^mysql_database=dbispconfig$/mysql_database=$MYSQL_DATABASE/g" "$INI"
    sed -i "s/^mysql_port=3306$/mysql_port=$MYSQL_PORT/g" "$INI"
}

patch_settings() {
    # I couldn't find these settings during install time, it didn't work,
    # so I just patched them
    local files=( /usr/local/ispconfig/interface/lib/config.inc.php /usr/local/ispconfig/server/lib/config.inc.php )
    for f in "${files[@]}"; do
        sed -i "s/^\$conf\\['db_user'\\] = .*/\$conf\\['db_user'\\] = '$MYSQL_USER';/" "$f"
        sed -i "s/^\$conf\\['db_password'\\] = .*/\$conf\\['db_password'\\] = '$MYSQL_PASSWORD';/" "$f"
    done
}

restore_db() {
    if [[ -f "$DB_DUMPFILE" ]]; then
        echo "Dump file detected! Restoring database..."
        if mysql -h"$MYSQL_HOST" -P"$MYSQL_PORT" -uroot -p"$MYSQL_ROOT_PASSWORD" "$MYSQL_DATABASE" < "$DB_DUMPFILE"; then
            echo "DB restored succesfully"
        else
            error "something failed during DB restoring process, please check the logs"
        fi
    fi
}

cleanup() {
    rm -rf "$TMPDIR"
}

create_dirs() {
    mkdir -p "${DIRS[@]}"
}

do_install() {
    echo "Installing ISPConfig..."
    sleep 20    # wait for DB
    download_files
    backup_db_and_drop  # The database must not exist
    # Change install options
    set_install_options "$TMPDIR/ispconfig3_install/install/autoinstall.ini"
    # Install
    echo "Executing ISPConfig installer..."
    php -q \
        "${TMPDIR}/ispconfig3_install/install/install.php" \
        --autoinstall="${TMPDIR}/ispconfig3_install/install/autoinstall.ini" \
        > "$INSTALL_LOG" 2>&1 \
            || bailout "ISPConfig didn't installed correctly"
    echo "Done"
    patch_settings
    restore_db
    cleanup
}

run() {
    supervisord -c /etc/supervisor/supervisord.conf
}

echo "Initializing ISPConfig container..."
create_dirs

if [[ ! -f "$INSTALL_LOG" ]]; then
    do_install
fi

echo "ISPConfig seems installed, running..."
run
